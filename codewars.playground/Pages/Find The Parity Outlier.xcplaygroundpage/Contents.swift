/*
 You are given an array (which will have a length of at least 3, but could be very large) containing integers. The array is either entirely comprised of odd integers or entirely comprised of even integers except for a single integer N. Write a method that takes the array as an argument and returns this "outlier" N.

Examples

[2, 4, 0, 100, 4, 11, 2602, 36]
Should return: 11 (the only odd number)

[160, 3, 1719, 19, 11, 13, -21]
Should return: 160 (the only even number).
*/
import Foundation

func findOutlier(_ array: [Int]) -> Int {
    var even = [Int]()
    var odd = [Int]()
    
    array.map {
        if $0 % 2 == 0 {
            even.append($0)
        } else {
            odd.append($0)
        }
    }
    
    if even.count > odd.count {
        return odd.first!
    } else {
        return even.first!
    }
}

findOutlier([2, 11, 0, 100, 4, 4, 2602, 36])
findOutlier([160, 3, 1719, 19, 11, 13, -21])
findOutlier([1, 33, 10053359313, 2, 1, 1, 1, 1, 1, 1, -3, 9])
findOutlier([8, 80, 14, 2, 20, 0, 21, 80])
