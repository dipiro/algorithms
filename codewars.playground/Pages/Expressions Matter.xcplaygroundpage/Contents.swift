/*
 Given three integers a ,b ,c, return the largest number obtained after inserting the following operators and brackets: +, *, ()
 In other words , try every combination of a,b,c with [*+()] , and return the Maximum Obtained

 With the numbers are 1, 2 and 3 , here are some ways of placing signs and brackets:

 1 * (2 + 3) = 5
 1 * 2 * 3 = 6
 1 + 2 * 3 = 7
 (1 + 2) * 3 = 9
 So the maximum value that you can obtain is 9.
 */
import Foundation

func expressionMatter(_ a: Int, _ b: Int, _ c: Int) -> Int {
    var array: [Int] = []
    
    let one = a * (b + c)
    let two = a * b * c
    let three = a + b * c
    let four = (a + b) * c
    let five = a + b + c
    print(one)
    print(two)
    print(three)
    print(four)
    print(five)

    array.append(contentsOf: [one, two, three, four, five])
    
    return array.max() ?? 0
}

expressionMatter(1, 2, 3)
expressionMatter(2, 2, 4)
expressionMatter(2, 1, 2)
expressionMatter(1, 10, 1)
