/*
 Simple, given a string of words, return the length of the shortest word(s).

 String will never be empty and you do not need to account for different data types.
 */
import Foundation

var text = "bitcoin take over the world maybe who knows perhaps"

func find_short(_ str: String) -> Int {
    return str.components(separatedBy: " ").map { $0.count}.min() as! Int
}

find_short(text)
