/*
 Usually when you buy something, you're asked whether your credit card number, phone number or answer to your most secret question is still correct. However, since someone could look over your shoulder, you don't want that shown on your screen. Instead, we mask it.
 
 Your task is to write a function maskify, which changes all but the last four characters into '#'.
 
 Examples
 
 maskify("4556364607935616") == "############5616"
 maskify(     "64607935616") ==      "#######5616"
 maskify(               "1") ==                "1"
 maskify(                "") ==                 ""
 
 // "What was the name of your first pet?"
 maskify("Skippy")                                   == "##ippy"
 maskify("Nananananananananananananananana Batman!") == "####################################man!"
 */


import Foundation

func maskify(_ string:String) -> String {
    
    guard string.count > 4 else { return string }
//        let firstStringCount = string.prefix(string.count - 4).count
//        let newString = String(repeating: "#", count: firstStringCount)
//        let lastString = string.suffix(4)
//        return newString + lastString
    
    return String(repeating: "#", count: string.count - 4) + string.dropFirst(string.count - 4)

}

maskify("4556364607935616")
maskify("64607935616")
maskify("1")
maskify("")
