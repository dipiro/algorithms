/*
 Write a function to get the first elements of asequence. Passing a parameter n (default=1) will return the first n elements of the sequence.

 If n == 0 return an empty sequence []

 Examples

 var arr = ['a', 'b', 'c', 'd', 'e'];
 first(arr) //=> ['a'];
 first(arr, 2) //=> ['a', 'b']
 first(arr, 3) //=> ['a', 'b', 'c'];
 first(arr, 0) //=> [];
 */

func first(array: [String], n: Int) -> [String] {
    
    guard n < array.count else { return [] }
    
    if n == 0 {
        return []
    } else if n == 1 {
        return Array(arrayLiteral: array.first!)
    } else {
        return Array(array.prefix(n))
    }

}
first(array: ["a", "b", "c", "d", "e"], n: 2)
first(array: ["a", "b", "c", "d", "e"], n: 0)
first(array: ["a", "b", "c", "d", "e"], n: 1)
first(array: ["a", "b", "c", "d", "e"], n: 10)
