/*
 Welcome. In this kata, you are asked to square every digit of a number and concatenate them.

 For example, if we run 9119 through the function, 811181 will come out, because 9ˆ2 is 81 and 1ˆ2 is 1.

 Note: The function accepts an integer and returns an integer
 */

import Foundation

func squareDigits(_ num: Int) -> Int {
    
    let x = String(num).map { String($0)}
    let y = x.map { Int($0)! }.map { String($0*$0) }.joined()
// return Int(String(num).compactMap(\.wholeNumberValue).map({String(Int(pow(Double($0), 2)))}).joined()) ?? 0
    return Int(y)!
}

squareDigits(9119)
squareDigits(9109)
