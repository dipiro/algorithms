/*
 Consider integer numbers from 0 to n - 1 written down along the circle in such a way that the distance between any two neighbouring numbers is equal (note that 0 and n - 1 are neighbouring, too).

 Given n and firstNumber/first_number, find the number which is written in the radially opposite position to firstNumber.

 Example

 For n = 10 and firstNumber = 2, the output should be

 circleOfNumbers(n, firstNumber) == 7
 */

import Foundation


func circleOfNumbers(_ n: Int, _ fst: Int) -> Int {
    let half = n / 2
    
    if half + fst >= n {
        return fst - half
    } else {
        return fst + half
    }
}

circleOfNumbers(10, 2) // 7
circleOfNumbers(10, 7) // 2
circleOfNumbers(4, 1)

