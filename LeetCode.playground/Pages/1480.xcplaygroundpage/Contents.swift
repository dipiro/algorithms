/*
 Given an array nums. We define a running sum of an array as runningSum[i] = sum(nums[0]…nums[i]).

Return the running sum of nums.

 

Example 1:

Input: nums = [1,2,3,4]
Output: [1,3,6,10]
Explanation: Running sum is obtained as follows: [1, 1+2, 1+2+3, 1+2+3+4].
Example 2:

Input: nums = [1,1,1,1,1]
Output: [1,2,3,4,5]
Explanation: Running sum is obtained as follows: [1, 1+1, 1+1+1, 1+1+1+1, 1+1+1+1+1].
Example 3:

Input: nums = [3,1,2,10,1]
Output: [3,4,6,16,17]
*/

class Solution {
    func runningSum(_ nums: [Int]) -> [Int] {
        var array = [Int]()
        var num = 0
        for index in nums {
            num += index
            array.append(num)
        }
        return array
    }
}

let x1 = Solution()
x1.runningSum([1,2,3,4])
x1.runningSum([1,1,1,1,1])
